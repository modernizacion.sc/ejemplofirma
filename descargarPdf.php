<?php

//Login
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://firmadigital.santacruz.gob.ar/firmar/v1/login',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '{
	"usuario": "Usuario",
  	"password": "Password"
}', CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'),
));

$response = curl_exec($curl);

curl_close($curl);
$array = json_decode($response, true);

$token = $array['access_token'];

$idtransaccion = $_GET['idTransaccion'];

$transacciones = file_get_contents("Transacciones/transaccion".$idtransaccion.".json");
$json = json_decode($transacciones, true);

$idArchivo= $json['idArchivo'];
//$idArchivo = 'id de archivo';

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://firmadigital.santacruz.gob.ar/firmar/v1/descargar',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_POSTFIELDS => '{
                "IdArchivo": "'.$idArchivo.'"
 }',
    CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . $token . '',
        'Content-Type: application/json',
    ),
));
$response = curl_exec($curl);
curl_close($curl);
$jsonResponse = json_decode($response, true);

var_dump($jsonResponse['success']);

if($jsonResponse['success'] == "0" ){
    header('Location: VerPdf.php?idArchivo='.$idArchivo);

}
else{

//la firma fue realizada con exito;
$file = "Pdfs_Firmados/$idArchivo.pdf";

$fp = fopen($file, "w");
fwrite($fp, $response);
fclose($fp);

header('Location: VerPdf.php?idArchivo='.$idArchivo);
}
