<!DOCTYPE html>

<html lang="es">

    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="container">
            
                <div class="card">
                    <div class="card-header text-center">
                        <h1>Ejemplo - Integración PFDR</h1>
                    </div>
                    <div class="card-body">
                        <nav>
                            <ul class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="ejemplo1-tab" data-toggle="tab"
                                        href="#ejemplo1">Ejemplo 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="ejemplo2-tab" data-toggle="tab"
                                        href="#ejemplo2">Ejemplo 2</a>
                                </li>
                            </ul>
                        </nav>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="ejemplo1" role="tabpanel"
                            aria-labelledby="nav-ejemplo1-tab">
                       
                                <form action="firmarPdf.php" method="POST" enctype="multipart/form-data">
                                    <div class="card">
                                    
                                        <div class="card-body">
                                        <fieldset>
                                            <legend>Datos Generales</legend>
                                            <div class="form-group">
                                                <label for="inputCuil">Cuil:</label>
                                                <input type="text" id="inputCuil" name="cuil" placeholder="Sin guiones medios" maxlength="13" autofocus required/>
                                            </div>
                                            </br>
                                            <div class="form-group">
                                                <label for="inputDocumento">Tipo de Documento:</label>
                                                    <select name="documento" id="inputDocumento" required>
                                                        <option value="PDF" selected>PDF</option>
                                                        <option value="HASH">HASH</option>
                                                    </select>
                                            </div>
                                            </fieldset>
                                            </br>
                                            <hr>
                                            <fieldset>
                                                <legend>Datos PDF</legend>
                                                <div class="form-group">
                                                <label for="inputCampo1">Campo 1:</label>
                                                <input id="inputCampo1" name="inputCampo1" type="text" placeholder="titulo">
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                <label for="inputCampo2">Campo 2:</label>
                                                <input id="inputCampo2" name="inputCampo2" type="text" placeholder="subtitulo"> 
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                <label for="inputCampo3">Campo 3:</label>
                                                <textarea id="inputCampo3" name="inputCampo3" rows="10" cols="50" placeholder="Contenido"></textarea>
                                            </div>
                                            </fieldset>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary"> Firmar </button>
                                        </div>
                                    </div>
                                </form>

                            </div>

                            <div class="tab-pane fade show" id="ejemplo2" role="tabpanel"
                            aria-labelledby="nav-ejemplo2-tab">
                         
                                <form action="firmar.php" method="POST" enctype="multipart/form-data">
                                    <div class="card">
                                    
                                        <div class="card-body">
                                        <fieldset>
                                            <legend>Datos Generales</legend>
                                            <div class="form-group">
                                                <label for="inputCuil">Cuil:</label>
                                                <input type="text" id="inputCuil" name="cuil" placeholder="Sin guiones medios" maxlength="13" autofocus required/>
                                            </div>
                                            </br>
                                            <div class="form-group">
                                                <label for="inputDocumento">Tipo de Documento:</label>
                                                    <select name="documento" id="inputDocumento" required>
                                                        <option value="PDF" selected>PDF</option>
                                                        <option value="HASH">HASH</option>
                                                    </select>
                                            </div>
                                            </fieldset>
                                            </br>
                                        <div class="form-group">
                                                <label for="inputFile">Enviar un nuevo archivo:</label>
                                                </br>
                                                </br>
                                                <input id="inputFile" type="file" name="file" required>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary"> Firmar </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
               
                    </div>
                      
                    </div>
                  
                </div>
          
        </div>

    </body>

</html>

<script>

$(document).ready(Principal);
    function Principal(){

        $(document).on('keyup','#inputCuil',function(e){
            if($(this).val().length == 2) {

                $(this).val($(this).val()+"-");

            }

            if($(this).val().length == 11) {

                $(this).val($(this).val()+"-");

            }
        });
    }

</script>
