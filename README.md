# Ejemplo de integración de la Plataforma de Firma Digital Remota (PFDR)

> Desarrollado por la Secretaría de Estado de Modernización e Innovación Tecnológica, Jefatura de Gabinete de Ministros, Provincia de Santa Cruz.


# Contenido
- [Descarga e Instalación](#descarga-e-instalación)
- [Documentación de Desarrollador](#documentación-de-desarrollador)



# Descarga e Instalación
A continuación se enumeran los pasos para la instalación del ejemplo de firma digital remota, bajo sistema operativo GNU/Linux y Microsoft Windows.

## Requerimientos

### Sistema operativo GNU/Linux (Verificado para Debian 9 y 10):
- Servidor Web Apache
- PHP 7.4+
- Git

### Sistema operativo Microsoft Windows:
- Servidor Web Apache
- PHP 7.4+
- Git

## Descarga del proyecto
```bash
$ cd ~
$ git clone https://gitlab.com/modernizacion.sc/ejemplofirma.git
```

## Acceso a la demostración en vivo

Para acceder a la demostración funcional del ejemplo, ingrese a la siguiente dirección desde el navegador de su preferencia:

```bash
https://modernizacion.santacruz.gob.ar/ejemploFirma/
```


# Documentación de Desarrollador

## Código fuente

> Una vez descargado el repositorio oficial, debe definir las variables `"usuario"` y `"password"`, dentro los archivos `firmarPdf.php`, `firmar.php` y `descargarPdf.php` para sucorrecta utilización.


## Colección Postman
Postman es una herramienta que permite crear y probar peticiones sobre APIs de forma sencilla. Puede obtenerse gratuitamente desde este enlace:
- [Descargar la aplicación POSTMAN](https://www.postman.com/downloads/)

Las rutas disponibles por defecto se encuentran agrupadas en la colección POSTMAN que se dispone:  `Firmar-Api - Usuarios.postman_collection.json`

> Dentro de Postman definir las variables `{{HOST}}`,`{{TOKEN}}` y `{{RTOKEN}}` para la correcta utilización de la colección.

## Instructivo de uso

En el documento que se ofrece `"Documentación Firmar-Api (Versión Usuarios).pdf"`, se especifican cada una de las funcionalidades ofrecidas en el API, como así también un diagrama de flujo del proceso de firma.
